# Import our main library
import PySimpleGUI as sg

global mod_data

mod_data = {
    "name": "Example mod",
    "author": "Your Name",
    "version": "1.0",
    "description": "This is a testing mod!",
    "dependencies": [],
    "load": "main",
    "uicreated": "onuicreated"
}

def onuicreated(atm):
    # Set as last tab
    index = 10**10 # Probably not the best way to do this, but it works
    atm["add_layout"]("Example Mod", [index,[[sg.Text("This is an example mod, go to mods/funnymod.py to see how it works!")]]])

def main(atm):
    print("This is a funny mod!")