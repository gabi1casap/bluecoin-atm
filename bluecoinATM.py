import PySimpleGUI as sg
import urllib3
import json
import os
import threading
import subprocess
import ctypes
import signal
import encryptlib
import importlib
import base64
import sys
import platform
if os.name == "nt":
    whnd = ctypes.windll.kernel32.GetConsoleWindow()
    # Check if the script is running in a console window or not
    if whnd != 0:
        # If it is, hide the console
        ctypes.windll.user32.ShowWindow(whnd, 0)
else:
    # if it is not windows, then it is probably linux, so we have to use a different method
    # to hide the console
    # Do we have an argument supplied?
    if not len(sys.argv) >= 0:   
        # We do not, so lets start a new process
        subprocess.Popen([sys.executable, "hide!"])
enc = encryptlib.HWIDBasedEncryption()
settings = None
# Open settings.json
with open('settings.json', 'r') as file:
    # Decode the JSON
    settings = json.load(file)


# Initialize urllib3 pool manager
http = urllib3.PoolManager()

# Set the theme and options for the window
theme = settings['Theme']
# Is it nothing?
if theme == "":
    # Set the theme to light grey
    theme = "DarkBlack"
    # Save the theme in the settings file
    settings["Theme"] = theme
    with open('settings.json', 'w') as file:
        json.dump(settings, file)
sg.theme(theme)
sg.set_options(font=("Helvetica", 17))

# Function to save user credentials
def save_user(username, password):

    if os.path.exists('users.json'):
        with open('users.json', 'r') as file:
            users = json.load(file)
    else:
        users = {}

    encrypted_password = enc.encrypt(password)
    # Convert bytes to a string using Base64
    encrypted_password_str = base64.b64encode(encrypted_password).decode('utf-8')

    users[username] = encrypted_password_str

    with open('users.json', 'w') as file:
        json.dump(users, file)

def update_user(username, password):
    # Load the users
    users = load_users()
    # Update the password
    encrypted_password = enc.encrypt(password)
    # Convert bytes to a string using Base64
    encrypted_password_str = base64.b64encode(encrypted_password).decode('utf-8')
    users[username] = encrypted_password_str
    # Save the users
    with open('users.json', 'w') as file:
        json.dump(users, file)

def load_users():

    if not os.path.exists('users.json'):
        return {}

    with open('users.json', 'r') as file:
        encrypted_users = json.load(file)

    decrypted_users = {}
    for user, password_str in encrypted_users.items():
        # Convert string back to bytes
        encrypted_password = base64.b64decode(password_str)
        decrypted_password = enc.decrypt(encrypted_password)
        decrypted_users[user] = decrypted_password

    return decrypted_users

# Function to send a request
def send_request(url, method, username, password, receiver=None, amount=None, new_password=None):
    data = {"Method": method, "Username": username, "Password": password}
    if receiver:
        data["ReceivingUser"] = receiver
    if amount:
        data["Amount"] = amount
    if new_password:
        data["NewPassword"] = new_password

    encoded_data = json.dumps(data).encode('utf-8')
    headers = {'Content-Type': 'application/json'}
    response = http.request('POST', url, body=encoded_data, headers=headers)
    return response.data.decode('utf-8')

global additional_layout

additional_layout = {}

global runonuicreation
runonuicreation = []

def save_settings():
    global settings
    with open('settings.json', 'w') as file:
        json.dump(settings, file)

def add_layout(name, layout):
    global additional_layout
    additional_layout[name] = layout
    
def remove_layout(name):
    global additional_layout
    additional_layout.pop(name)

def modify_layout(name, idx, layout):
    global additional_layout
    additional_layout[name][idx] = layout

def modify_layout_element(name, idx, element, value):
    global additional_layout
    additional_layout[name][idx][element] = value

# Function to create the main window
def create_main_window():
    
    for func in runonuicreation:
        func(library)
    # Layout for Login Tab
    login_layout = [
        [sg.Text('', expand_x=True),sg.Text("Login", font=("Helvetica", 30), justification="center"), sg.Text('', expand_x=True)],
        [sg.Text("Username"), sg.InputText(key='Username')],
        [sg.Text("Password"), sg.InputText(key='Password', password_char='*')],
        [sg.Button("Select new user")]
    ]

    # Layout for Signup Tab
    signup_layout = [
        [sg.Text('', expand_x=True),sg.Text("Signup", font=("Helvetica", 30), justification="center"), sg.Text('', expand_x=True)],
        [sg.Text("Username"), sg.InputText(key='Signup_Username')],
        [sg.Text("Password"), sg.InputText(key='Signup_Password', password_char='*')],
        [sg.Button("Signup")]
    ]

    # Layout for Transaction Tab
    transaction_layout = [
        [sg.Text('', expand_x=True),sg.Text("Transactions", font=("Helvetica", 30), justification="center"), sg.Text('', expand_x=True)],
        [sg.Text("Enter recipient"), sg.InputText(key='Transaction_Recipient')],
        [sg.Text("Enter amount"), sg.InputText(key='Transaction_Amount')],
        [sg.Button("Make Transaction")]
    ]

    # Layout for Check Balance Tab
    management_layout = [
        [sg.Text('', expand_x=True),sg.Text("Account Management", font=("Helvetica", 30), justification="center"), sg.Text('', expand_x=True)],
        [sg.Button("Check Balance"), sg.Button("Change Password", key='ChangePassword')]
    ]
    mining_layout = [
        [sg.Text('', expand_x=True),sg.Text("Mining", font=("Helvetica", 30), justification="center"), sg.Text('', expand_x=True)],
        [sg.Button("Start Mining", key='StartMining'), sg.Button("Stop Mining", key='StopMining')],
        [sg.Multiline(size=(40, 10), key='MiningOutput', disabled=True, autoscroll=True)]
    ]
    
    donation_layout = [
        [sg.Text('', expand_x=True),sg.Text("Donation", font=("Helvetica", 30), justification="center"), sg.Text('', expand_x=True)],
        [sg.Text("Enter donation amount"), sg.InputText(key='Donation_Amount')],
        [sg.Button("Donate!")]
    ]
    
    if settings["Theme"] == "LightGrey":
        default_theme = "Light"
    elif settings["Theme"] == "DarkBlack":
        default_theme = "Dark"
    else:
        default_theme = "Dark"
    
    settings_layout = [
        # Big centered text, saying settings
        [sg.Text('', expand_x=True),sg.Text("Settings", font=("Helvetica", 30), justification="center"), sg.Text('', expand_x=True)],
        [sg.Text("Running version: " + settings['Version'] + " with operating system: " + platform.system() + " " + platform.release())],
        # Mod list
        [sg.Text("Mods:")],
        [sg.Listbox(list({}), size=(31, 10), key='ModList', select_mode=sg.LISTBOX_SELECT_MODE_MULTIPLE, enable_events=True, bind_return_key=True)], # Empty listbox
        [sg.Text("Theme"), sg.Combo(['Light', 'Dark'], default_value=default_theme, key='Theme', readonly=True)],
        [sg.Button("Apply Theme")]
        # Dropdown to select the version
    ]

    # Define the window's tabs
    tab_group_layout = [
        sg.Tab('Login', login_layout),
        sg.Tab('Signup', signup_layout),
        sg.Tab('Transaction', transaction_layout),
        sg.Tab('Manage account', management_layout),
        sg.Tab('Mining', mining_layout),
        sg.Tab('Donate', donation_layout),
        sg.Tab('Settings', settings_layout)
    ]
    print(additional_layout)
    for layoutname, layout in additional_layout.items():
        tab_group_layout.insert(layout[0], sg.Tab(layoutname, layout[1])) # layout[0] is for the order, layout[1] is for the layout itself

    # Create the window
    return sg.Window(f"Bluecoin ATM {settings['Version']}", [[sg.TabGroup([tab_group_layout])]])

# Function to show user selection layout
def user_selection_layout(users):
    return [[sg.Listbox(list(users.keys()), size=(31, 10), key='UserList')],
            [sg.Button("Select User"), sg.Button("New User"), sg.Button("Delete User")]]

def get_user():
    # Is the miner running? STOP IT!!
    stop_mining()
    
    users = load_users()
    user_window = sg.Window("User Selection", user_selection_layout(users))
    sg.theme(settings['Theme'])
    selected_user = None

    while True:
        event, values = user_window.read()
        

        if event == sg.WIN_CLOSED:
            user_window.close()
            return "No User Selected"

        if event == "Select User":
            if len(values['UserList']) == 0:
                sg.popup("Please select a user or create a new one", title="No User Selected")
                continue
            selected_user = values['UserList'][0]
            break

        if event == "New User":
            selected_user = None
            break
        if event == "Delete User":
            # Delete the user
            if len(values['UserList']) == 0:
                sg.popup("Please select a user to delete", title="No User Selected")
                continue
            
            selected_user = values['UserList'][0]
            users.pop(selected_user)
            with open('users.json', 'w') as file:
                json.dump(users, file)
            # Reload the user list
            users = load_users()
            user_window['UserList'].update(list(users.keys()))
        

    user_window.close()
    return selected_user

global handlers
def testHandler(event, values):
    print(event, values)
    #sg.popup("Test handler called", title="Test Handler")
    return False # Dont stop execution
handlers = {
    "TestHandler":testHandler
}

def addHandler(name, handler):
    global handlers
    handlers[name] = handler

def removeHandler(name):
    global handlers
    handlers.pop(name)


global mining_process
global process
mining_process = None
stop_mining_flag = threading.Event()
stop_mining_flag.clear()
def start_mining(username, window):
    # Does ./bluecoinMINER.exe exist?
    if not os.path.exists("bluecoinMINER.exe") or not os.path.exists("bluecoinMINER"): # Check for both windows and linux
        sg.popup("bluecoinMINER.exe not found, (gitlab version does not have integrated miner).", title="Error")
        return
    stop_mining() #Incase user clicks start mining twice
    global mining_process, stop_mining_flag

    def run():
        global process
        stop_mining_flag.clear()
        if os.name == "nt":
            process = subprocess.Popen(["bluecoinMINER.exe", username], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, bufsize=1)
        else:
            process = subprocess.Popen(["./bluecoinMINER", username], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, bufsize=1)
        while not stop_mining_flag.is_set():
            line = process.stdout.readline()
            if not line:
                break
            window.write_event_value('-MINING_OUTPUT-', line)
        print("i stop??")
        process.wait()  # Wait for the process to finish
        process.stdout.close()

    mining_process = threading.Thread(target=run, daemon=True)
    mining_process.start()

def stop_mining():
    global stop_mining_flag, mining_process
    stop_mining_flag.set()
    if mining_process:
        os.kill(process.pid, signal.SIGTERM)  # Send a signal to the process
        # we dont need to wait for thread, probably gonna finish soonish
        mining_process = None

global username
global password
username = ""
password = ""

def get_user_mod():
    return [username, password]

def main():
    users = load_users()  # Load users at the beginning of the main function
    sg.theme(settings['Theme'])
    selected_user = get_user()
    if selected_user == "No User Selected":
        return "STOP"

    window = create_main_window()

    user_valid = threading.Event()
    def check_user(username, password):
        if username == "" or password == "":
            user_valid.clear()
            return
        response = send_request("https://darkbluestealth.pythonanywhere.com", "VerifyUser", username, password)
        print(response)
        if response == "True":
            user_valid.set()
        else:
            user_valid.clear()



    full_output = ""
    # Firstly we check if user is valid
    selected_username, selected_password = selected_user, users.get(selected_user)
    valid = check_user(selected_username, selected_password)
    window.Finalize()
    olduser = ""
    oldpassword = ""
    oldnewuser = ""
    oldnewpassword = ""
    if selected_user and not window['Username'].get() and not window['Password'].get():
        window['Username'].update(selected_username)
        window['Password'].update(selected_password)
    if valid:
        window['Username'].update(background_color='green')
        window['Password'].update(background_color='green')
    else:
        window['Username'].update(background_color='red')
        window['Password'].update(background_color='red')
        
    # Set the mod list
    window['ModList'].update(list(mods))
    window['ModList'].bind('<Double-Button-1>', 'DoubleClicked')
    while True:
        
        event, values = window.read(timeout=0.05)
        if event == "ModList":
            continue # Ignore this event
        if event == "ModListDoubleClicked":
            # Get the selected mod
            if len(values['ModList']) == 0:
                continue
            selected_mod = values['ModList'][0]
            # Get the mod data
            mod_data = mods[selected_mod]
            # Display the mod data
            sg.popup(f"Name: {mod_data['name']}\nAuthor: {mod_data['author']}\nVersion: {mod_data['version']}\nDescription: {mod_data['description']}", title="Mod Info")
            continue
        if event == "Select new user":
            window.close()
            break  # Exit the loop and close the window

        if event in (sg.WIN_CLOSED, "Cancel"):
            break
        global username
        global password
        username = values['Username']
        password = values['Password']
        # check if signup username and password are the same as the old ones
        newusername = values['Signup_Username']
        newpassword = values['Signup_Password']
        if username != olduser or password != oldpassword:
            olduser = username
            oldpassword = password
            user_valid.clear()
            threading.Thread(target=check_user, args=(username, password)).start()
        
        if newusername != oldnewuser or newpassword != oldnewpassword:
            oldnewuser = newusername
            oldnewpassword = newpassword
            if newusername == "" or newpassword == "":
                continue
            # Ignore for now
            
        
        olduser = username
        oldpassword = password
        
        
        if user_valid.is_set():
            window['Username'].update(background_color='green')
            window['Password'].update(background_color='green')
            # Does user already exist in user list?
            if username not in users:
                # If not, add them, and save the user list
                save_user(username, password)
                # Reload the user list
                users = load_users()    
        else:
            window['Username'].update(background_color='red')
            window['Password'].update(background_color='red')
        
        
        # Theme selection
        if event == "Apply Theme":
            # Get the theme from the dropdown
            lightTheme = "LightGrey"
            darkTheme = "DarkBlack"
            theme = values['Theme']
            if theme == "Light":
                sg.theme(lightTheme)
                settings["Theme"] = lightTheme
            elif theme == "Dark":
                sg.theme(darkTheme)
                settings["Theme"] = darkTheme
            # Save the theme in the settings file
            with open('settings.json', 'w') as file:
                json.dump(settings, file)
            # Regenerate the window
            window.close()
            return None
        if event == sg.TIMEOUT_KEY:
            continue
        
        for handlername, handler in handlers.items():
            if handler(event, values):
                continue # Handler returned true, so we should not execute the rest of the code

        if event == "Signup":
            signup_username = values['Signup_Username']
            signup_password = values['Signup_Password']
            response = send_request("https://darkbluestealth.pythonanywhere.com", "Signup", signup_username, signup_password)
            if response == "400":
                sg.popup("User already exists", title="Error")
            elif response == "200":
                sg.popup("Account created successfully", title="Success")
                save_user(signup_username, signup_password)
                users = load_users()  # Reload users
                # Set the username and password fields to the new user
                window['Username'].update(signup_username)
                window['Password'].update(signup_password)
            else:
                sg.popup("Error: " + response)
            continue
        
        if not user_valid.is_set():
            sg.popup("Username or password invalid.", title="Error")
            continue
        if event == 'StopMining':
            stop_mining()  # Call the function to stop mining
        if event == 'StartMining':
            username = values.get('Username')
            if username:
                start_mining(username, window)
            else:
                sg.popup("Please enter a username to start mining.", title="No username entered")

        if event == '-MINING_OUTPUT-':
            output = values[event]

            if '\r' in output:
                # Split the output on carriage returns
                lines = output.split('\r')
                for line in lines:
                    # Remove the last line from full_output
                    full_output = '\n'.join(full_output.split('\n')[:-1])
                    # Add the new line
                    full_output += line + '\n'
            else:
                # Append the line to full_output
                full_output += output

        # Update the Multiline element with the modified full_output
        window['MiningOutput'].update(value=full_output)
        
        if event == "Donate!":
            # Get amount
            amount = values['Donation_Amount']
            # Send request
            response = send_request("https://darkbluestealth.pythonanywhere.com", "Transaction", username, password, "GenericRblxStudioDev", amount)
            if response == "404":
                sg.popup("Your password is invalid", title="Error")
            elif response == "400":
                sg.popup("Invalid username.", title="Error")
            elif response == "407":
                sg.popup("Invalid recipient username.", title="Error")
            elif response == "402":
                sg.popup("You cannot take bluecoin, silly.", title="Error")
            elif response == "401":
                sg.popup("You don't have enough bluecoin.", title="Error")
            elif response == "200":
                sg.popup(f"Thanks for donating {amount} bluecoin to me! Your new balance is {send_request('https://darkbluestealth.pythonanywhere.com', 'CheckBalance', username, password)}", title="Thanks for donating") # Check balance should work
                

        if event == "Make Transaction":
            receiver = values['Transaction_Recipient']
            amount = values['Transaction_Amount']
            response = send_request("https://darkbluestealth.pythonanywhere.com", "Transaction", username, password, receiver, amount)
            if response == "404":
                sg.popup("Your password is invalid", title="Error")
            elif response == "400":
                sg.popup("Invalid username.", title="Error")
            elif response == "407":
                sg.popup("Invalid recipient username.", title="Error")
            elif response == "402":
                sg.popup("You cannot take bluecoin, silly.", title="Error")
            elif response == "401":
                sg.popup("You don't have enough bluecoin.", title="Error")
            elif response == "200":
                sg.popup("Transaction successful, new balance: " + send_request("https://darkbluestealth.pythonanywhere.com", "CheckBalance", username, password), title="Success") # Check balance should work
            else:
                sg.popup("Error: " + response)
        
        elif event == "ChangePassword":
            # Ask the user for new password
            new_password = sg.popup_get_text("Enter new password", password_char='*', )
            if new_password:
                # Send the request
                response = send_request("https://darkbluestealth.pythonanywhere.com", "ChangePassword", username, password, new_password=new_password)
                if response == "407":
                    sg.popup("Invalid username.", title="Error")
                elif response == "408":
                    sg.popup("Invalid password.", title="Error")
                elif response == "100":
                    sg.popup("Password changed successfully.", title="Success")
                    # Update the password in the user list
                    users[username] = new_password
                    # Save the user list
                    update_user(username, new_password)
                    
                    # Update the password field
                    window['Password'].update(new_password)
                else:
                    sg.popup("Error: " + response)
                

        elif event == "Check Balance":
            response = send_request("https://darkbluestealth.pythonanywhere.com", "CheckBalance", username, password)
            if response == "E407":
                sg.popup("Invalid username.", title="Error")
            elif response == "E406":
                sg.popup("Your password is invalid", title="Error")
            else:
                # Modify response for non scientific notation (e.g. 1e+06)
                response = '{:.8f}'.format(float(response))
                sg.popup("Your balance is: " + response, title="Balance")

        #Regenerate the window if needed
        if selected_user and not window['Username'].get() and not window['Password'].get():
            window['Username'].update(selected_username)
            window['Password'].update(selected_password)
            
mods = {}
library = {
    "add_layout":add_layout,
    "remove_layout":remove_layout,
    "modify_layout":modify_layout,
    "modify_layout_element":modify_layout_element,
    "addHandler":addHandler,
    "removeHandler":removeHandler,
    "send_request":send_request,
    "settings":settings,
    "save_settings":save_settings,
    "load_users":load_users,
    "get_current_user":get_user_mod,
    "save_user":save_user,
    "update_user":update_user
}

loaded = []

def runmod(scriptname):
    # Is it already loaded?
    for mod in loaded:
        if mod.mod_data["name"] == scriptname:
            # It is, so we dont need to load it
            return
    # Import the module
    mod = importlib.import_module("mods." + scriptname)
    # Loop over dependencies
    for dependency in mod.mod_data["dependencies"]:
        # Import the dependency
        runmod(dependency)
    mainfunc = getattr(mod, "main")
    # Run the main function, and pass some of the functions from this file
    mainfunc(library)
    # Add it to loaded
    loaded.append(mod)
    runonuicreation.append(getattr(mod, mod.mod_data["uicreated"]))
    mods[mod.mod_data["name"]] = mod.mod_data


if __name__ == "__main__":
    # Import every module in the mods folder that ends with .py
    for file in os.listdir("mods"):
        if file.endswith(".py"):
            print("DEBUG: Imported " + file[:-3])
            runmod(file[:-3])
    while True:
        data = main()
        if data == "STOP":
            break
        # Restart the program